// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MyTopDownShooterGameMode.h"
#include "MyTopDownShooterPlayerController.h"
#include "MyTopDownShooter/Character/MyTopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTopDownShooterGameMode::AMyTopDownShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTopDownShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}